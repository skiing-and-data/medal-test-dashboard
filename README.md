# Medal test dashboard

An R shiny application to show UW Nordic Medal test results.

## About the Medal Test

The Medal Test is designed to help gauge fitness for Cross Country Skiing. No one activity can indicate how you will ski but success in this test shows a physical readiness for ski training & technique. Data taken over 20 years shows a strong correlation between the total score on this test and race performance as measured by USSA/FIS points. Of course ski technique, experience on snow, and race experience will influence your final results in addition to fitness. The primary goal in doing this test is to find an athlete's strengths and weaknesses so they can work on both of these. This dashboard assists athletes in comparing their results over time.

## Authors

This dashboard is created and managed by Ella DeWolf
edewolf@uwyo.edu
